# Changelog

## 1.0.2
- wrapped first toggle() call in new Future() for moving it to a later execution point
- added check in toggle() whether element with given id is available

## 0.0.1

- Initial version
