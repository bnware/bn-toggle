library bn_toggle.component;

import 'package:angular/angular.dart';
import 'dart:html';
import 'dart:async';

@Decorator(
    selector: "[bn-toggle]"
)
class BnToggle implements AttachAware {

  @NgAttr('bn-toggle')
  String id;

  String error = "";
  Element node;


  BnToggle(this.node) {
  }


  @override
  void attach() {
    InputElement input;

    try {
      input = node as InputElement;
    } catch (exception) {
      print("Decorator can only be applied to input elements!");
      return;
    }

    if (!id.startsWith("#")) {
      id = "#$id";
    }

    new Future(() => toggle(input));

    input.onChange.listen((event) {
      toggle(input);
    });
  }

  void toggle(InputElement input) {
    var elem = document.querySelector(id);
    if (!input.checked && elem != null) {
      elem.attributes["disabled"] = "disabled";
      elem.style.display = "none";
    } else {
      elem.attributes.remove("disabled");
      elem.style.display = "block";

      //get all other radios of this group to disable them, if this element is a radio button
      if (input.type == "radio") {
        var radios = document.querySelectorAll(
            'input[type="radio"][name="${input.name}"]');
        radios
            .where((radio) => radio.id != input.id)
            .forEach((InputElement radio) {
          radio.dispatchEvent(new Event("change", canBubble: false));
        });
      }
    }
  }

}